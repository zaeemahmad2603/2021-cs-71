﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Class_Attendance : Form
    {
        public Class_Attendance()
        {
            InitializeComponent();
        }

        private void Class_Attendance_Load(object sender, EventArgs e)
        {
            panel1.BackColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into ClassAttendance values (@AttendanceDate)", con);
            /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
            cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Added");
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = txt_id.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM ClassAttendance WHERE Id='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE ClassAttendance SET AttendanceDate ='" + dateTimePicker1.Value + "' WHERE Id = '" + txt_id.Text + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Edited");
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE Id='" + txt_id.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();
            this.Close();
        }
    }
}
