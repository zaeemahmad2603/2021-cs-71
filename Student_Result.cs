﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Student_Result : Form
    {
        public Student_Result()
        {
            InitializeComponent();
        }

        private void Student_Result_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.White;
            label1.ForeColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            GetStudentId();
            GetAssessmentComponentId();
            GetRubricMeasurementId();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@StudentId, @AssessmentComponentId, @RubricMeasurementId, @EvaluationDate)", con);
                /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                cmd.Parameters.AddWithValue("@StudentId", cmb_studentid.Text);
                cmd.Parameters.AddWithValue("@AssessmentComponentId", cmb_assessmentcomponentid.Text);
                cmd.Parameters.AddWithValue("@RubricMeasurementId", cmb_rubricmeasurementid.Text);
                cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
            }
            catch
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = cmb_studentid.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM StudentResult WHERE StudentId='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE StudentResult SET AssessmentComponentId='" + cmb_assessmentcomponentid.Text + "', RubricMeasurementId='" + cmb_rubricmeasurementid.Text + "', EvaluationDate='" + dateTimePicker1.Value + "' WHERE StudentId = '" + cmb_studentid.Text + "'", con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            catch
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM StudentResult WHERE StudentId='" + cmb_studentid.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();*/
            this.Close();
        }

        private void GetStudentId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Student Where status='" + 5 + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_studentid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }

        private void GetAssessmentComponentId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_assessmentcomponentid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }

        private void GetRubricMeasurementId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_rubricmeasurementid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }
    }
}
