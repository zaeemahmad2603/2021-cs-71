﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Assessment_Component : Form
    {
        public Assessment_Component()
        {
            InitializeComponent();
        }

        private void Assessment_Component_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.White;
            label1.ForeColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            GetRubricId();
            GetAssessmentId();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(txt_totalmarks.Text) >= 0)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name, @RubricId, @TotalMarks, @DateCreated, @DateUpdated, @AssessmentId)", con);
                    /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                    cmd.Parameters.AddWithValue("@Name", txt_name.Text);
                    cmd.Parameters.AddWithValue("@RubricId", cmb_rubricid.Text);
                    cmd.Parameters.AddWithValue("@TotalMarks", txt_totalmarks.Text);
                    cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value);
                    cmd.Parameters.AddWithValue("@DateUpdated", dateTimePicker2.Value);
                    cmd.Parameters.AddWithValue("@AssessmentId", cmb_assessmentid.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Added");
                }
                else
                {
                    MessageBox.Show("Please Enter the valid total marks for assessment component.");
                }
            }
            catch
            {
                MessageBox.Show("Please Enter the valid information as :- \n ID is integer. \n Name is string. \n Total Marks are integers.");
            }
        }
         
        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = txt_id.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM AssessmentComponent WHERE Id='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(txt_totalmarks.Text) >= 0)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("UPDATE AssessmentComponent SET Name='" + txt_name.Text + "', RubricId='" + cmb_rubricid.Text + "', TotalMarks='" + txt_totalmarks.Text + "', DateCreated='" + dateTimePicker1.Value + "', DateUpdated='" + dateTimePicker2.Value + "', AssessmentId='" + cmb_assessmentid.Text + "' WHERE Id = '" + txt_id.Text + "'", con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Edited");
                }
                else
                {
                    MessageBox.Show("Please Enter the valid total marks for assessment component.");
                }
            }
            catch
            {
                MessageBox.Show("Please Enter the valid information as :- \n ID is integer. \n Name is string. \n Total Marks are integers.");

            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Id='" + txt_id.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();*/
            this.Close();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GetAssessmentId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_assessmentid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }

        private void GetRubricId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_rubricid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }
    }
}
