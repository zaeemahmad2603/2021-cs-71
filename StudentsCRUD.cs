﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class StudentsCRUD : Form
    {
        public StudentsCRUD()
        {
            InitializeComponent();
        }

        private void StudentsCRUD_Load(object sender, EventArgs e)
        {
            panel1.BackColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber, @Status)", con);
            /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
            cmd.Parameters.AddWithValue("@FirstName", txt_firstname.Text);
            cmd.Parameters.AddWithValue("@LastName", txt_lastname.Text);
            cmd.Parameters.AddWithValue("@Contact", txt_contact.Text);
            cmd.Parameters.AddWithValue("@Email", txt_email.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", txt_registrationnumber.Text);
            cmd.Parameters.AddWithValue("@Status", txt_status.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Added");
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = txt_id.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE Id='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Student SET RegistrationNumber='" + txt_registrationnumber.Text + "', FirstName='" + txt_firstname.Text + "', LastName='" + txt_lastname.Text + "', Status='" + txt_status.Text + "', Contact='" + txt_contact.Text + "', Email='" + txt_email.Text + "' WHERE Id = '" + txt_id.Text + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Edited");
        }

        private void btn_search_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Id='" + txt_id.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void txt_id_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txt_firstname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_lastname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_contact_TextChanged(object sender, EventArgs e)
        {
            Regex regex = new Regex("^\\+?\\d{1,4}?[-.\\s]?\\(?\\d{1,3}?\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,9}$");
            Match matching = regex.Match(txt_contact.Text.ToString());
            if (matching.Success)
            {
                lbl_check_contact.Text = "Valid";
                lbl_check_contact.ForeColor = Color.Green;
            }
            else
            {
                lbl_check_contact.Text = "InValid";
                lbl_check_contact.ForeColor = Color.Red;
            }
        }

        private void txt_email_TextChanged(object sender, EventArgs e)
        {
            Regex regex = new Regex("^\\S+@\\S+\\.\\S+$");
            Match matching = regex.Match(txt_email.Text.ToString());
            if (matching.Success)
            {
                lbl_check_email.Text = "Valid";
                lbl_check_email.ForeColor = Color.Green;
            }
            else
            {
                lbl_check_email.Text = "InValid";
                lbl_check_email.ForeColor = Color.Red;
            }
        }

        private void txt_registrationnumber_TextChanged(object sender, EventArgs e)
        {
            string input = txt_registrationnumber.Text;

            var dashCounter = input.Count(x => x == '-');

            if(dashCounter == 2)
            {
                lbl_check_registrationnumber.Text = "Valid";
                lbl_check_registrationnumber.ForeColor = Color.Green;
            }
            else
            {
                lbl_check_registrationnumber.Text = "InValid";
                lbl_check_registrationnumber.ForeColor = Color.Red;
            }
        }

        private void txt_status_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();
            this.Close();
        }
    }
}
