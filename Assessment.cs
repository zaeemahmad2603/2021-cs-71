﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Assessment : Form
    {
        public Assessment()
        {
            InitializeComponent();
        }

        private void Assessment_Load(object sender, EventArgs e)
        {
            panel1.BackColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();
            this.Close();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (float.Parse(txt_totalmarks.Text) >= 0 && float.Parse(txt_totalweightage.Text) >= 0)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title, @DateCreated, @TotalMarks, @TotalWeightage)", con);
                /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                cmd.Parameters.AddWithValue("@Title", txt_title.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value);
                cmd.Parameters.AddWithValue("@TotalMarks", txt_totalmarks.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", txt_totalweightage.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
            }
            else
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = txt_id.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM Assessment WHERE Id='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (float.Parse(txt_totalmarks.Text) >= 0 && float.Parse(txt_totalweightage.Text) >= 0)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title='" + txt_title.Text + "', DateCreated='" + dateTimePicker1.Value + "', TotalMarks='" + txt_totalmarks.Text + "', TotalWeightage='" + txt_totalweightage.Text + "' WHERE Id = '" + txt_id.Text + "'", con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE Id='" + txt_id.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void txt_totalmarks_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(txt_totalmarks.Text) >= 0)
            {
                //lbl_check_totalmarks.Text = "Valid";
                //lbl_check_totalmarks.ForeColor = Color.Green;
            }
            else
            {
                //lbl_check_totalmarks.Text = "InValid";
                //lbl_check_totalmarks.ForeColor = Color.Red;
            }
        }

        private void txt_totalweightage_TextChanged(object sender, EventArgs e)
        {
            if (int.Parse(txt_totalweightage.Text) >= 0)
            {
                //lbl_check_totalweightage.Text = "Valid";
                //lbl_check_totalweightage.ForeColor = Color.Green;
            }
            else
            {
                //lbl_check_totalweightage.Text = "InValid";
                //lbl_check_totalweightage.ForeColor = Color.Red;
            }
        }

        private void txt_totalmarks_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
