﻿using iTextSharp.text;
using iTextSharp;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Drawing.Text;

namespace Mids_Project
{
    public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.White;
            label1.ForeColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            btn_generatereport.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            lbl_attendanceid.Visible = false;
            cmb_attendanceid.Visible = false;
            dataGridView2.Visible = false;
            GetAttendanceId();
            GetAttendanceInformation();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();*/
            this.Close();
        }

        private void btn_generatereport_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Assessment Wise Students Result")
            {
                exportAssessmentWiseResultToPdf(dataGridView1, "test");
            }
            else if (comboBox1.Text == "Clo Wise Students Result")
            {
                exportCloWiseResultToPdf(dataGridView1, "test");
            }
            else if (comboBox1.Text == "Attendance Report")
            {
                exportAttendanceReportToPdf(dataGridView1, "test");
            }



            /*Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            PdfWriter.GetInstance(pdfDoc, new FileStream("Table.pdf", FileMode.Create));
            pdfDoc.Open();

            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);
            table.DefaultCell.Padding = 3;
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell(new Phrase(dataGridView1.Columns[i].HeaderText));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                table.AddCell(cell);
            }

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1.Rows[i].Cells[j].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1.Rows[i].Cells[j].Value.ToString()));
                    }
                }
            }

            pdfDoc.Add(table);
            pdfDoc.Close();*/

            /*if (dataGridView1.Rows.Count > 0)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF (*.pdf)|*.pdf";
                save.FileName = "Result.pdf";
                bool ErrorMessage = false;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                    {
                        try
                        {
                            File.Delete(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            ErrorMessage = true;
                            MessageBox.Show("Unable to write the data in disk"+ex.Message);
                        }
                    }
                    if (!ErrorMessage)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(dataGridView1.Columns.Count);
                            pTable.DefaultCell.Padding = 2;
                            pTable.WidthPercentage= 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach(DataGridViewColumn col in dataGridView1.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(col.HeaderText));
                                pTable.AddCell(pCell);
                            }
                            foreach(DataGridViewRow viewRow in dataGridView1.Rows)
                            {
                                foreach(DataGridViewCell dcell in viewRow.Cells)
                                {
                                    pTable.AddCell(dcell.Value.ToString());
                                }
                            }

                            using (FileStream filestream = new FileStream(save.FileName,FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                document.Open();
                                document.Add(pTable);
                                document.Close();
                                filestream.Close();
                            }
                            MessageBox.Show("Data exports successfully","info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error while exporting the Data"+ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No record found", "info");
            }*/



            /*printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDocument1.Print();
            }*/
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            /*e.Graphics.DrawString("COLD DRINK DISTRIBUTORS", new Font("Arial", 22, FontStyle.Bold), Brushes.Black, 10, 125);
            e.Graphics.DrawString("Name : XYZ", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 210);
            e.Graphics.DrawString("Date : ................", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 580, 210);
            e.Graphics.DrawString("Department : ABC", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 260);
            e.Graphics.DrawString("Desigination : Employee / Rider / Manager", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 310);
            e.Graphics.DrawString("Basic Pay :00000000", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 390);
            e.Graphics.DrawString("Tax : 10%", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 410, 390);
            e.Graphics.DrawString("Allowance :20%", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 440);
            e.Graphics.DrawString("Pay : 00000", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, 560, 490);*/
        }

        public void exportAssessmentWiseResultToPdf(DataGridView dgw, string filename)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            PdfPTable pdftable = new PdfPTable(dgw.Columns.Count);
            pdftable.DefaultCell.Padding = 3;
            pdftable.WidthPercentage= 100;
            pdftable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdftable.DefaultCell.BorderWidth = 1;

            iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);

            
            

            foreach(DataGridViewColumn column in dgw.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText,text));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdftable.AddCell(cell);
            }

            foreach(DataGridViewRow row in dgw.Rows)
            {
                foreach(DataGridViewCell cell in row.Cells)
                {
                    pdftable.AddCell(new Phrase(cell.Value.ToString()));
                }
            }

            var savefiledialoge = new SaveFileDialog();
            savefiledialoge.FileName = filename;
            savefiledialoge.DefaultExt = ".pdf";
            if(savefiledialoge.ShowDialog()==DialogResult.OK)
            {
                using (FileStream stream = new FileStream(savefiledialoge.FileName, FileMode.Create))
                {
                    Document pdfdoc = new Document(PageSize.A4,10f,10f,10f,0f);
                    PdfWriter.GetInstance(pdfdoc, stream);
                    pdfdoc.Open();

                    Paragraph heading = new Paragraph("Assessment Wise Class Result");
                    heading.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 48f);
                    heading.Alignment = Element.ALIGN_CENTER;

                    pdfdoc.Add(heading);


                    pdfdoc.Add(pdftable);
                    pdfdoc.Close();
                    stream.Close();
                }
            }
        }


        public void exportCloWiseResultToPdf(DataGridView dgw, string filename)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            PdfPTable pdftable = new PdfPTable(dgw.Columns.Count);
            pdftable.DefaultCell.Padding = 3;
            pdftable.WidthPercentage = 100;
            pdftable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdftable.DefaultCell.BorderWidth = 1;

            iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
            foreach (DataGridViewColumn column in dgw.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText, text));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdftable.AddCell(cell);
            }

            foreach (DataGridViewRow row in dgw.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdftable.AddCell(new Phrase(cell.Value.ToString()));
                }
            }

            var savefiledialoge = new SaveFileDialog();
            savefiledialoge.FileName = filename;
            savefiledialoge.DefaultExt = ".pdf";
            if (savefiledialoge.ShowDialog() == DialogResult.OK)
            {
                using (FileStream stream = new FileStream(savefiledialoge.FileName, FileMode.Create))
                {
                    Document pdfdoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    PdfWriter.GetInstance(pdfdoc, stream);
                    pdfdoc.Open();

                    Paragraph heading = new Paragraph("CLO Wise Class Result");
                    heading.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 48f);
                    heading.Alignment = Element.ALIGN_CENTER;

                    pdfdoc.Add(heading);

                    pdfdoc.Add(pdftable);
                    pdfdoc.Close();
                    stream.Close();
                }
            }
        }

        public void exportAttendanceReportToPdf(DataGridView dgw, string filename)
        {
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
            PdfPTable pdftable = new PdfPTable(dgw.Columns.Count);
            pdftable.DefaultCell.Padding = 3;
            pdftable.WidthPercentage = 100;
            pdftable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdftable.DefaultCell.BorderWidth = 1;

            iTextSharp.text.Font text = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
            foreach (DataGridViewColumn column in dgw.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText, text));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(240, 240, 240);
                pdftable.AddCell(cell);
            }

            foreach (DataGridViewRow row in dgw.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdftable.AddCell(new Phrase(cell.Value.ToString()));
                }
            }

            var savefiledialoge = new SaveFileDialog();
            savefiledialoge.FileName = filename;
            savefiledialoge.DefaultExt = ".pdf";
            if (savefiledialoge.ShowDialog() == DialogResult.OK)
            {
                using (FileStream stream = new FileStream(savefiledialoge.FileName, FileMode.Create))
                {
                    Document pdfdoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    PdfWriter.GetInstance(pdfdoc, stream);
                    pdfdoc.Open();

                    Paragraph heading = new Paragraph("Attendance Report");
                    heading.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 48f);
                    heading.Alignment = Element.ALIGN_CENTER;

                    Paragraph heading1 = new Paragraph("      ");
                    heading.Font = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 48f);
                    heading.Alignment = Element.ALIGN_CENTER;

                    pdfdoc.Add(heading);
                    pdfdoc.Add(heading1);

                    pdfdoc.Add(pdftable);
                    pdfdoc.Close();
                    stream.Close();
                }
            }
        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.Text == "Attendance Report")
            {
                lbl_attendanceid.Visible = true;
                cmb_attendanceid.Visible = true;
                dataGridView2.Visible = true;
            }
            else
            {
                lbl_attendanceid.Visible = false;
                cmb_attendanceid.Visible = false;
                dataGridView2.Visible = false;
            }
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            if (comboBox1.Text == "Assessment Wise Students Result")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Assessment.Id as AssessmentId, Student.RegistrationNumber, AssessmentComponent.Id as ComponentId, AssessmentComponent.TotalMarks as ComponentMarks,  RubricLevel.MeasurementLevel as RubricMeasurementLevel, StudentResult.EvaluationDate, Cast(Cast(RubricLevel.MeasurementLevel as decimal(10,2))/4*AssessmentComponent.TotalMarks as decimal(10,2)) as ObtainedMarks, Concat(Cast((Cast(Cast(RubricLevel.MeasurementLevel as decimal(10,2))/4*AssessmentComponent.TotalMarks as decimal(10,2))*100/AssessmentComponent.TotalMarks) as decimal(10,2)),'%') as Percentage From Assessment Join AssessmentComponent on Assessment.Id = AssessmentComponent.AssessmentId Join StudentResult on AssessmentComponent.Id = StudentResult.AssessmentComponentId Join RubricLevel on StudentResult.RubricMeasurementId = RubricLevel.Id Join Student on StudentResult.StudentId = Student.Id\r\n", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if (comboBox1.Text == "Clo Wise Students Result")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Clo.Id as CloId, Student.RegistrationNumber, AssessmentComponent.Name, RubricLevel.MeasurementLevel as RubricMeasurementLevel, AssessmentComponent.TotalMarks as ComponentMarks, StudentResult.EvaluationDate, Cast(Cast(RubricLevel.MeasurementLevel as decimal(10,2))/4*AssessmentComponent.TotalMarks as decimal(10,2)) as ObtainedMarks, Concat(Cast((Cast(Cast(RubricLevel.MeasurementLevel as decimal(10,2))/4*AssessmentComponent.TotalMarks as decimal(10,2))*100/AssessmentComponent.TotalMarks) as decimal(10,2)),'%') as Percentage From Clo Join Rubric on Clo.Id = Rubric.CloId Join RubricLevel on Rubric.Id = RubricLevel.RubricId Join StudentResult on StudentResult.RubricMeasurementId = RubricLevel.Id Join AssessmentComponent on StudentResult.AssessmentComponentId = AssessmentComponent.Id Join Student on StudentResult.StudentId = Student.Id\r\n", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            else if(comboBox1.Text == "Attendance Report")
            {
                string attendanceid = cmb_attendanceid.Text;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Student.RegistrationNumber, Student.FirstName+' '+Student.LastName as StudentName, Lookup.Name as AttendanceStatus From StudentAttendance Join Student on Student.Id = StudentAttendance.StudentId Join Lookup on Lookup.LookupId = StudentAttendance.AttendanceStatus Where StudentAttendance.AttendanceId ='" + attendanceid + "'", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GetAttendanceId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_attendanceid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }

        private void GetAttendanceInformation()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }
    }
}
