﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Students_Attendance : Form
    {
        public Students_Attendance()
        {
            InitializeComponent();
        }

        private void Students_Attendance_Load(object sender, EventArgs e)
        {
            panel1.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            //GetStudentId();
            GetClassAttendanceId();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            /*var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;*/
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();
            this.Close();
        }

        private void GetClassAttendanceId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_attendanceid.DataSource = idList;

            cmd.ExecuteNonQuery();
            /*MessageBox.Show("Successfully Searched");*/
        }
        private void GetStudentId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_studentid.DataSource = idList;

            cmd.ExecuteNonQuery();
            /*MessageBox.Show("Successfully Searched");*/
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.Columns["Present"].Index == e.ColumnIndex)
                {
                    string valueid = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    string studentit = cmb_studentid.Text;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId, @StudentId, @AttendanceStatus)", con);
                    /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                    cmd.Parameters.AddWithValue("@AttendanceId", cmb_attendanceid.Text);
                    cmd.Parameters.AddWithValue("@StudentId", valueid);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", 1);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Attendance Marked Successfully");
                }
                else if (dataGridView1.Columns["Absent"].Index == e.ColumnIndex)
                {
                    string valueid = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    string studentit = cmb_studentid.Text;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId, @StudentId, @AttendanceStatus)", con);
                    /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                    cmd.Parameters.AddWithValue("@AttendanceId", cmb_attendanceid.Text);
                    cmd.Parameters.AddWithValue("@StudentId", valueid);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", 2);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Attendance Marked Successfully");
                    /*this.BackColor = Color.Green;*/
                }
                else if (dataGridView1.Columns["Leave"].Index == e.ColumnIndex)
                {
                    string valueid = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    string studentit = cmb_studentid.Text;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId, @StudentId, @AttendanceStatus)", con);
                    /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                    cmd.Parameters.AddWithValue("@AttendanceId", cmb_attendanceid.Text);
                    cmd.Parameters.AddWithValue("@StudentId", valueid);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", 3);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Attendance Marked Successfully");
                    /*this.BackColor = Color.Blue;*/
                }
                else if (dataGridView1.Columns["Late"].Index == e.ColumnIndex)
                {
                    string valueid = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    string studentit = cmb_studentid.Text;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId, @StudentId, @AttendanceStatus)", con);
                    /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                    cmd.Parameters.AddWithValue("@AttendanceId", cmb_attendanceid.Text);
                    cmd.Parameters.AddWithValue("@StudentId", valueid);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", 4);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Attendance Marked Successfully");
                    /*this.BackColor = Color.Yellow;*/
                }
            }
            catch
            {
                /*Exception ex = new Exception();
                MessageBox.Show(ex);*/
                MessageBox.Show("You cannot update the attendance of the student, as you have already mark the attendance of this student earlier.");
            }
        }

       /* private void GV_InventoryList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string id = GV_InventoryList.Rows[e.RowIndex].Cells["ID1"].FormattedValue.ToString();

            if (id != "")
            {
                Bottles bottles = Stock_DL.findStockIntoList(id);

                if (GV_InventoryList.Columns["Edit"].Index == e.ColumnIndex)
                {
                    var editForm = new InventoryEdit(bottles);
                    editForm.ShowDialog();
                    DataBind();
                }
                else if (GV_InventoryList.Columns["Delete"].Index == e.ColumnIndex)
                {
                    Stock_DL.deleteStockIntoList(id);
                    Stock_DL.dataBindingList();
                    DataBind();
                    MessageBox.Show("Stock Remove Permanetly");
                }


            }
        }*/
    }
}
