﻿namespace Mids_Project
{
    partial class MainPortalTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_attendance = new System.Windows.Forms.Button();
            this.btn_enrollments = new System.Windows.Forms.Button();
            this.btn_courses = new System.Windows.Forms.Button();
            this.btn_students = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(499, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "Home";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(903, 107);
            this.panel1.TabIndex = 2;
            // 
            // btn_attendance
            // 
            this.btn_attendance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_attendance.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_attendance.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_attendance.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_attendance.FlatAppearance.BorderSize = 0;
            this.btn_attendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attendance.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attendance.ForeColor = System.Drawing.Color.White;
            this.btn_attendance.Location = new System.Drawing.Point(0, 174);
            this.btn_attendance.Name = "btn_attendance";
            this.btn_attendance.Size = new System.Drawing.Size(232, 61);
            this.btn_attendance.TabIndex = 0;
            this.btn_attendance.Text = "Assessment";
            this.btn_attendance.UseVisualStyleBackColor = false;
            // 
            // btn_enrollments
            // 
            this.btn_enrollments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_enrollments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_enrollments.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_enrollments.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_enrollments.FlatAppearance.BorderSize = 0;
            this.btn_enrollments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_enrollments.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_enrollments.ForeColor = System.Drawing.Color.White;
            this.btn_enrollments.Location = new System.Drawing.Point(0, 117);
            this.btn_enrollments.Name = "btn_enrollments";
            this.btn_enrollments.Size = new System.Drawing.Size(232, 57);
            this.btn_enrollments.TabIndex = 0;
            this.btn_enrollments.Text = "CLO";
            this.btn_enrollments.UseVisualStyleBackColor = false;
            // 
            // btn_courses
            // 
            this.btn_courses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_courses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_courses.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_courses.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_courses.FlatAppearance.BorderSize = 0;
            this.btn_courses.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_courses.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_courses.ForeColor = System.Drawing.Color.White;
            this.btn_courses.Location = new System.Drawing.Point(0, 57);
            this.btn_courses.Name = "btn_courses";
            this.btn_courses.Size = new System.Drawing.Size(232, 60);
            this.btn_courses.TabIndex = 0;
            this.btn_courses.Text = "Students Result";
            this.btn_courses.UseVisualStyleBackColor = false;
            this.btn_courses.Click += new System.EventHandler(this.btn_courses_Click);
            // 
            // btn_students
            // 
            this.btn_students.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_students.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_students.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_students.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn_students.FlatAppearance.BorderSize = 0;
            this.btn_students.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_students.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_students.ForeColor = System.Drawing.Color.White;
            this.btn_students.Location = new System.Drawing.Point(0, 0);
            this.btn_students.Name = "btn_students";
            this.btn_students.Size = new System.Drawing.Size(232, 57);
            this.btn_students.TabIndex = 0;
            this.btn_students.Text = "Students";
            this.btn_students.UseVisualStyleBackColor = false;
            this.btn_students.Click += new System.EventHandler(this.btn_students_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Controls.Add(this.btn_attendance);
            this.panel3.Controls.Add(this.btn_enrollments);
            this.panel3.Controls.Add(this.btn_courses);
            this.panel3.Controls.Add(this.btn_students);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 107);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(232, 411);
            this.panel3.TabIndex = 6;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox2.Image = global::Mids_Project.Properties.Resources.Home_Page_Picture;
            this.pictureBox2.Location = new System.Drawing.Point(288, 150);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(556, 327);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::Mids_Project.Properties.Resources.home_page_home_picture;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(232, 107);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(232, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 107);
            this.button1.TabIndex = 3;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainPortalTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(903, 518);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "MainPortalTeacher";
            this.Text = "MainPortalTeacher";
            this.Load += new System.EventHandler(this.MainPortalTeacher_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_attendance;
        private System.Windows.Forms.Button btn_enrollments;
        private System.Windows.Forms.Button btn_courses;
        private System.Windows.Forms.Button btn_students;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}