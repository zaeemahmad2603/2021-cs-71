﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Rubric_Level : Form
    {
        public Rubric_Level()
        {
            InitializeComponent();
        }

        private void Rubric_Level_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.White;
            label1.ForeColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            GetRubricId();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@RubricId, @Details, @MeasurementLevel)", con);
                /*cmd.Parameters.AddWithValue("@Id", txt_id.Text);*/
                cmd.Parameters.AddWithValue("@RubricId", cmb_rubricid.Text);
                cmd.Parameters.AddWithValue("@Details", rtxt_details.Text);
                cmd.Parameters.AddWithValue("@MeasurementLevel", cmb_measurementlevel.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Added");
            }
            catch
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            string delete_name = txt_id.Text;
            SqlCommand cmd = new SqlCommand("DELETE FROM RubricLevel WHERE Id='" + delete_name + "'", con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE RubricLevel SET RubricId='" + cmb_rubricid.Text + "', Details='" + rtxt_details.Text + "', MeasurementLevel='" + cmb_measurementlevel.Text + "' WHERE Id = '" + txt_id.Text + "'", con);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            catch 
            {
                MessageBox.Show("Please Enter the valid information.");
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE Id='" + txt_id.Text + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Searched");
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();*/
            this.Close();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GetRubricId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            /*dataGridView1.DataSource = dt;*/

            List<int> idList = new List<int>();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = reader.GetInt32(0); // Get the value of the first column (in this case, 'id')
                    idList.Add(id); // Add the value to the list
                }
            }
            cmb_rubricid.DataSource = idList;

            cmd.ExecuteNonQuery();
        }
    }
}
