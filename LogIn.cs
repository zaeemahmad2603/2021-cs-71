﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }
      
        private void LogIn_Load(object sender, EventArgs e)
        {
            Color color = ThemeColor.SelectThemeColor(this);
            ThemeColor.PrimaryColor = color;
            btn_signin.BackColor = color;   
            btn_signup.BackColor = color;
            btn_signup.ForeColor = Color.White;
            btn_signin.ForeColor = Color.White;
            lbl_username.BackColor = color;
            lbl_password.BackColor = color;
            lbl_username.ForeColor = Color.White;
            lbl_password.ForeColor = Color.White;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_signin_Click(object sender, EventArgs e)
        {
            if (txt_username.Text == "Teacher" && txt_password.Text == "asdf1234")
            {
                this.Hide();
                MessageBox.Show("You successfully sign in into the system.");
                MainPortalTeacher mpt = new MainPortalTeacher();
                mpt.ShowDialog();
            }
            else
            {
                MessageBox.Show("Enter the valid information.");
            }
            
        }

        private void btn_signup_Click(object sender, EventArgs e)
        {
            SignUp signup = new SignUp();
            signup.ShowDialog();
        }
    }
}
