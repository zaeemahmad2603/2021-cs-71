﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.White;
            label1.ForeColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
            btn_generatereport.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            /*this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();*/
            this.Close();
        }

        private void btn_generatereport_Click(object sender, EventArgs e)
        {
            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawString("COLD DRINK DISTRIBUTORS", new Font("Arial", 22, FontStyle.Bold), Brushes.Black, 10, 125);
            e.Graphics.DrawString("Name : XYZ", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 210);
            e.Graphics.DrawString("Date : ................", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 580, 210);
            e.Graphics.DrawString("Department : ABC", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 260);
            e.Graphics.DrawString("Desigination : Employee / Rider / Manager", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 310);
            e.Graphics.DrawString("Basic Pay :00000000", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 390);
            e.Graphics.DrawString("Tax : 10%", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 410, 390);
            e.Graphics.DrawString("Allowance :20%", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, 10, 440);
            e.Graphics.DrawString("Pay : 00000", new Font("Arial", 12, FontStyle.Bold), Brushes.Black, 560, 490);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Assessment Wise Students Result")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Assessment.Id as Assessment, AssessmentComponent.Id as Components, AssessmentComponent.TotalMarks, StudentResult.StudentId, StudentResult.RubricMeasurementId, RubricLevel.MeasurementLevel, StudentResult.EvaluationDate, Cast(Cast(RubricLevel.MeasurementLevel/4 as decimal(10,2))*AssessmentComponent.TotalMarks as decimal(10,2)) as ObtainedMarks From Assessment Join AssessmentComponent on Assessment.Id = AssessmentComponent.AssessmentId Join StudentResult on AssessmentComponent.Id = StudentResult.AssessmentComponentId Join RubricLevel on StudentResult.RubricMeasurementId = RubricLevel.Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
            }
        }
    }
}
