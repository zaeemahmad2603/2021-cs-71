﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mids_Project
{
    public partial class CURD_Clo : Form
    {
        public CURD_Clo()
        {
            InitializeComponent();
        }

        private void CURD_Clo_Load(object sender, EventArgs e)
        {
            panel1.BackColor = ThemeColor.PrimaryColor;
            btn_add.BackColor = ThemeColor.PrimaryColor;
            btn_delete.BackColor = ThemeColor.PrimaryColor;
            btn_update.BackColor = ThemeColor.PrimaryColor;
            btn_search.BackColor = ThemeColor.PrimaryColor;
            btn_show.BackColor = ThemeColor.PrimaryColor;
            btn_back.BackColor = ThemeColor.PrimaryColor;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainPortalTeacher mpt = new MainPortalTeacher();
            mpt.ShowDialog();
            this.Close();
        }
    }
}
